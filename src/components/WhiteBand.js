import React, { Component } from 'react'

class WhiteBand extends Component {
    constructor() {
        super();
        this.state = {
            middleKid: 4,
            counter: 0,
            order: [1, 2, 3, 4, 5]
        }
    }

    StartMachine(number) {
        const { whiteBandIndex } = this.props;
        const father = document.getElementById(whiteBandIndex + '-wrapper')

        // Creates a spin move at every 80ms interval
        let interval = setInterval(() => {
            this.state.counter++

            // After 80 spins check if the middle slot is the passed number otherwise keep spinning until it is
            if (this.state.middleKid == number && this.state.counter > 80) {
                clearInterval(interval);
                this.state.counter = 0;
                if (typeof this.props.DisplayResult === 'function') {
                    this.props.DisplayResult()
                }
            } else {
                this.Spin(father)
            }
        }, 80)
    }

    // Every last slot will be moved to be the first one creating a spining move
    Spin(father) {
        const kids = father.childNodes
        father.classList.add('animate');
        kids[4].classList.add('hide')
        setTimeout(() => {
            for (var i = 0; i < kids.length; i++) {
                kids[i].classList.remove('hide')
            }
            father.insertBefore(kids[4], kids[0]);
            this.setState({ middleKid: kids[2].dataset.number })
        }, 40)
    }

    // Randomize the initial position of the slots on the white band
    Shuffle() {
        let { order } = this.state;
        var currentIndex = order.length, temporaryValue, randomIndex;
        while (0 !== currentIndex) {
            randomIndex = Math.floor(Math.random() * currentIndex);
            currentIndex -= 1;
            temporaryValue = order[currentIndex];
            order[currentIndex] = order[randomIndex];
            order[randomIndex] = temporaryValue;
        }
        this.setState({ order });
    }

    componentDidMount() {
        this.Shuffle();
    }

    render() {
        const { order } = this.state;
        const { whiteBandIndex, slots } = this.props;
        return (
            <div className={"white-band " + whiteBandIndex}>
                <div id={whiteBandIndex + "-wrapper"}>
                    {
                        Object.values(order).map((index) => {
                            return (
                                <img key={index} data-number={index} className={"slot-icon slot-icon-" + index} src={"/" + slots.find(x => x.index === index).img} />
                            )
                        })
                    }
                </div>
            </div>
        );
    }
}

export default WhiteBand;