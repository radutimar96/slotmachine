import React, { Component } from 'react'
import WhiteBand from './WhiteBand';

class SlotMachine extends Component {
    constructor() {
        super();
        this.WhiteBandOne = React.createRef();
        this.WhiteBandTwo = React.createRef();
        this.WhiteBandThree = React.createRef();
        this.state = {
            spinDisabled: false,
            gameInitialized: false,
            inputValue: 100,
            spinValue: 0,
            winSound: new Audio('/sounds/win.mp3'),
            lossSound: new Audio('/sounds/loss.mp3'),
            spinSound: new Audio('/sounds/spin.mp3'),
            startSound: new Audio('/sounds/start.mp3'),
            slotIcons: [
                { index: 1, img: 'Terminal.svg' },
                { index: 2, img: 'Clock.svg' },
                { index: 3, img: 'Server.svg' },
                { index: 4, img: 'Knoxon.svg' },
                { index: 5, img: 'Router.svg' },
            ],
            winningNumbers: [111, 222, 333, 444, 555],
            allNumbers: []
        }
    }

    StartMachine() {
        const { allNumbers, spinSound } = this.state;
        spinSound.play();

        // Taking a random number out the 100 
        const number = allNumbers[Math.floor(Math.random() * 100)]

        // Spliting it into an array to pass it to each WhiteBand
        const numberParsed = number.toString(10).replace(/\D/g, '0').split('').map(Number)

        this.setState({ spinDisabled: true, spinValue: number })
        
        // Starting the machine by starting each whiteband with a 500ms delay
        this.WhiteBandOne.current.StartMachine(numberParsed[0]);
        setTimeout(() => {
            this.WhiteBandTwo.current.StartMachine(numberParsed[1]);
        }, 500)
        setTimeout(() => {
            this.WhiteBandThree.current.StartMachine(numberParsed[2]);
        }, 1000);
    }

    DisplayResult() {
        const { winningNumbers, inputValue, spinValue, winSound, lossSound, spinSound } = this.state;
        const temp_value = inputValue;
        spinSound.pause();
        spinSound.currentTime = 0;
        if (winningNumbers.indexOf(spinValue) >= 0) {
            winSound.play();
        } else {
            lossSound.play();
        }

        this.setState({ inputValue: (winningNumbers.indexOf(spinValue) >= 0 ? 'Won' : 'Loss') }, () => {
            setTimeout(() => {
                this.setState({
                    inputValue: temp_value,
                    spinDisabled: false
                })
            }, 2000)
        })
    }

    componentDidMount() {
        const slotNo = 5;
        let allNumbers = [];

        // Creating an array with all the possible outcomes
        // 5 numbers out of 125 are winning numbers, that's 4% win chance
        for (let i = 1; i <= slotNo; ++i) {
            for (let j = 1; j <= slotNo; ++j) {
                for (let k = 1; k <= slotNo; ++k) {
                    allNumbers.push(parseInt(`${i}${j}${k}`))
                }
            }
        }

        let losingNumbers = allNumbers;
        const { winningNumbers } = this.state;

        // Creating the losing numbers array with 120 losing numbers 
        for (let number in winningNumbers) {
            const numberIndex = losingNumbers.indexOf(winningNumbers[number]);
            if (numberIndex !== -1) losingNumbers.splice(numberIndex, 1);
        }

        // Shuffling the losing numbers array and then reducing them to 95 losing numbers instead of 120
        losingNumbers = this.Shuffle(losingNumbers)
        losingNumbers.length = 95

        // Creating the allNumbers array consisting of 100 numbers ( 5 winning, 95 losing ), that's 5% win chance
        allNumbers = [...losingNumbers, ...winningNumbers]
        this.setState({ allNumbers })
    }

    InitializeMachine() {
        this.state.startSound.play();
        this.setState({ gameInitialized: true })
    }

    Shuffle(array) {
        var currentIndex = array.length, temporaryValue, randomIndex;
        while (0 !== currentIndex) {
            randomIndex = Math.floor(Math.random() * currentIndex);
            currentIndex -= 1;
            temporaryValue = array[currentIndex];
            array[currentIndex] = array[randomIndex];
            array[randomIndex] = temporaryValue;
        }
        return array
    }

    ChangeInputValue(value) {
        this.setState({ inputValue: value });
    }

    render() {
        const { slotIcons, gameInitialized, spinDisabled, inputValue } = this.state;
        return (
            <div className="machine">
                <div className="machine-header">
                    <div className="input-start-wrapper">
                        <input className="input-start" value={inputValue} onChange={(e) => this.ChangeInputValue(e.target.value)} type="string" disabled={gameInitialized} />
                        <button className="input-start-btn" onClick={() => { this.InitializeMachine() }} disabled={gameInitialized}></button>
                    </div>
                </div>
                <div className="machine-frame">
                    <WhiteBand slots={slotIcons} whiteBandIndex="white-band-one" ref={this.WhiteBandOne} />
                    <WhiteBand slots={slotIcons} whiteBandIndex="white-band-two" ref={this.WhiteBandTwo} />
                    <WhiteBand slots={slotIcons} whiteBandIndex="white-band-three" ref={this.WhiteBandThree} DisplayResult={() => this.DisplayResult()} />
                </div>
                <div className="machine-footer">
                    <button className="spin-button" onClick={() => this.StartMachine()} disabled={!gameInitialized || spinDisabled}></button>
                </div>
            </div>
        );
    }
}

export default SlotMachine;