import React from 'react';
import './App.scss';
import SlotMachine from './components/SlotMachine'

function App() {
  return (
    <div className="App">
      <SlotMachine></SlotMachine>      
    </div>
  );
}

export default App;
